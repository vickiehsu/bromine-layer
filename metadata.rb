name             "bromine-layer"
maintainer       "Qantas"
license          "All rights reserved"
description      "Testing application layer for bromine"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"

depends "aws", "= 2.5.0"
